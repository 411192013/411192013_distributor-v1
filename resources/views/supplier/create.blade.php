@extends('template')

@section('content')
<div class="row mt-5 mb-5">
    <div class="col-lg-12 margin-tb">
        <div class="float-left">
            <h2>Tambah Data Supplier Distributor </h2>
        </div>
        <div class="float-right">
            <a class="btn btn-secondary" href="{{ route('supplier.index') }}"> Back</a>
        </div>
    </div>
</div>

@if ($errors->any())
    <div class="alert alert-danger">
        <strong>Input Gagal !!!!!!!!!!!</strong>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<form action="{{ route('supplier.store') }}" method="POST">
    @csrf

     <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>KODE SUPPLIER :</strong>
                <input type="text" maxlength="10" name="kode_supplier" class="form-control" placeholder="P001">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>NAMA SUPPLIER:</strong>
                <input type="text" maxlength="200" name="nama_supplier" class="form-control" placeholder="Nama supplier"  >
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>ALAMAT:</strong>
                <textarea class="form-control" style="height:150px" name="alamat" placeholder="Text...."></textarea>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>NO TLP:</strong>
                <input type="text" name="no_telepon" class="form-control" placeholder="no_telepon"  >
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </div>

</form>
@endsection