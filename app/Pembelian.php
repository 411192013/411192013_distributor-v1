<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pembelian extends Model
{
    protected $table = 'pembelian';
    protected $primarykey = "id";

    protected $fillable = [
        'no_pembelian',
        'tanggal',
        'id_supplier',
        'id_barang',
        'jumlah_barang',
        'harga_barang',
        'created_by',
];
}