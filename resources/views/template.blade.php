<!DOCTYPE html>
<html lang="en">
<head>
  <!-- DataTables -->
  <link rel="stylesheet" href="{{ asset('plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
  <link rel="stylesheet" href="{{ asset('plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
  <link rel="stylesheet" href="{{ asset('plugins/datatables-buttons/css/buttons.bootstrap4.min.css')}}">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>UAS FULLSTACK DINI FEBRIANTI- 411192013</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@48,400,0,0" />
</head>
<body style="background: lightyellow">
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <div class="container-fluid">
    <a class="navbar-brand" href="{{ url('/') }}">HOME</a>
    <div class="container card-body">
            <a href="{{ route('barang.index') }}" class="btn btn-sm btn-warning">Data Barang</a>
            <a href="{{ route('pelanggan.index') }}" class="btn btn-sm btn-warning">Data Pelanggan</a>
            <a href="{{ route('penjualan.index') }}" class="btn btn-sm btn-warning">Data Penjualan</a>
            <a href="{{ route('pembelian.index') }}" class="btn btn-sm btn-warning">Data pembelian</a>
            <a href="{{ route('supplier.index') }}" class="btn btn-sm btn-warning">Data Supplier</a>

    </div>
  </div>
</nav>
<br>
    <div class="container">
        @yield('content')
    </div>
</body>
</html>