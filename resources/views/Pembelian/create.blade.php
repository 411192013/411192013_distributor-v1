@extends('template')

@section('content')
<div class="row mt-5 mb-5">
    <div class="col-lg-12 margin-tb">
        <div class="float-left">
            <h2>Tambah Data Pembelian</h2>
        </div>
        <div class="float-right">
            <a class="btn btn-secondary" href="{{ route('pembelian.index') }}"> Back</a>
        </div>
    </div>
</div>

@if ($errors->any())
    <div class="alert alert-danger">
        <strong>Input Gagal !!!!!!!!!!!</strong>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<form action="{{ route('pembelian.store') }}" method="POST">
    @csrf

     <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>NO PEMBELIAN :</strong>
                <input type="text" maxlength="10" name="no_pembelian" class="form-control" placeholder="P0001">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>tanggal:</strong>
                <input type="date" maxlength="200" name="tanggal" class="form-control" placeholder="tanggal"  >
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>ID SUPLLIER:</strong>
                <input type="number" name="id_supplier" class="form-control" placeholder="ID supplier"  >
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group{{ $errors->has('id_barang') ? ' alert alert-danger' : '' }}">
                                    <strong>ID Barang:</strong>
                                    <select name="id_barang" class="form-control @error('id_barang') is-invalid @enderror">
                                      <option value="">-- PILIH --</option>
                                      @foreach ($barang as $item)
                                        <option value="{{ $item->id }}" {{ old('id_barang') == $item->id ? 'selected' : null }}>
                                          {{ $item->nama_barang }}
                                        </option>
                                      @endforeach
                                    </select>
                                    @if($errors->has('id_barang'))
                                    <span class="help-block">
                                      <strong>{{ $errors->first('id_barang') }}</strong>
                                    </span>
                                    @endif
                                </div>
                  </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>JUMLAH BARANG:</strong>
                <input type="number" name="jumlah_barang" class="form-control" placeholder="jumlah barang"  >
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>HARGA BARANG:</strong>
                <input type="number" name="harga_barang" class="form-control" placeholder="harga barang"  >
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </div>

</form>
@endsection