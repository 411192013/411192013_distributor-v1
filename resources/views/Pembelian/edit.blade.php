@extends('template')

@section('content')
    <div class="row mt-5 mb-5">
        <div class="col-lg-12 margin-tb">
            <div class="float-left">
                <h2>Edit Pembelian Distributor</h2>
            </div>
            <div class="float-right">
                <a class="btn btn-secondary" href="{{ route('pembelian.index') }}"> Back</a>
            </div>
        </div>
    </div>

    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Input Gagal !!!!</strong>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <form action="{{ route('pembelian.update',$pembelian->id) }}" method="POST">
        @csrf
        @method('PUT')

        <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>NO PEMBELIAN:</strong>
                <input type="text" maxlength="10" name="no_pembelian" class="form-control" placeholder="NO PEMBELIAN" value="{{ $pembelian->no_pembelian }}">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>TANGGAL:</strong>
                <input type="text" name="tanggal" class="form-control" placeholder="TANGGAL" value="{{ $pepembelian->tanggal }}">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>ID SUPPLIER:</strong>
                <textarea class="form-control" style="height:150px" name="id_supplier" placeholder="Content">{{ $pemebelian->id_supplier }}</textarea>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>ID BARANG:</strong>
                <input type="text" name="id_barang" class="form-control" placeholder="id_barang" value="{{ $pembelian->id_barang}}">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>JUMLAH BARANG:</strong>
                <input type="text" name="jumlah_barang" class="form-control" placeholder="100.000" value="{{ $pembelian->jumlah_barang }}">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>HARGA BARANG:</strong>
                <input type="text" name="harga_barang" class="form-control" placeholder="100.000" value="{{ $pembelian->harga_barang }}">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
            <button type="submit" class="btn btn-primary">Update</button>
        </div>
    </div>

    </form>
@endsection