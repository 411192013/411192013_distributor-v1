@extends('template')

@section('content')
    <div class="row mt-5 mb-5">
        <div class="col-lg-12 margin-tb">
            <div class="float-left">
                <h2> DATA PEMBELIAN DISTRIBUTOR</h2>
            </div>
            <div class="float-right">
                <a class="btn btn-secondary" href="{{ route('pembelian.index') }}"> Back</a>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>NO PEMBELIAN:</strong>
                {{ $pembelian->no_pembelian }}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>TANGGAL :</strong>
                {{ $pembelian->tanggal }}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>ID SUPLLIER:</strong>
                {{ $pembelian->id_supplier }}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>ID BARANG:</strong>
                {{ $pembelian->id_barang }}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>JUMLAH BARANG:</strong>
                {{ $pembelian->jumlah_barang }}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>HARGA BARANG:</strong>
                {{ $pembelian->harga_barang }}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Dibuat pada tanggal:</strong>
                {{ $pembelian->created_at }}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Di Ubah Pada tanggal:</strong>
                {{ $pembelian->updated_at }}
            </div>
        </div>
    </div>
@endsection