<?php

namespace App\Http\Controllers;

use App\Barang;
use App\Penjualan;
use App\Pembelian;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class BarangController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $barang = Barang::select('barang.id', 'barang.kode_barang', 'barang.nama_barang', 'barang.deskripsi',
        'barang.stok_barang', 'barang.harga_barang',
        DB::raw('IFNULL(SUM(penjualan.jumlah_barang),0) AS QTY_Penjualan'),
        DB::raw('IFNULL(SUM(penjualan.jumlah_barang),0) AS QTY_Pembelian'))
        ->leftJoin('penjualan', 'barang.id', '=', 'penjualan.kode_barang')
        ->leftJoin('pembelian', 'barang.id', '=', 'pembelian.id_barang')
        ->groupBy('barang.kode_barang', 
        'barang.id', 'barang.nama_barang', 'barang.deskripsi', 'barang.stok_barang', 'barang.harga_barang')
        ->paginate(5);

        return view('barang.index', compact('barang'))->with('i', (request()->input('page',1)-1)*5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('barang.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'kode_barang' => [
                'required',
                Rule::unique('barang')->ignore($request->input('id_barang'))
            ],
            'nama_barang' => 'required',
            'deskripsi' => 'required',
            'stok_barang' => 'required',
            'harga_barang' => 'required',
        ]);
  
        Barang::create($request->all());
   
        return redirect()->route('barang.index')->with('success','barang created successfully.');
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\barang  $barang
     * @return \Illuminate\Http\Response
     */
    public function show(barang $barang)
    {
        return view('barang.show', compact('barang'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\barang  $barang
     * @return \Illuminate\Http\Response
     */
    public function edit(barang $barang)
    {
        return view('barang.edit', compact('barang'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\barang  $barang
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, barang $barang)
    {
        $request->validate([
            'kode_barang' => 'required',
            'nama_barang' => 'required',
            'deskripsi' => 'required',
            'stok_barang' => 'required',
            'harga_barang' => 'required',
        ]);

        $barang->update($request->all());
        return redirect()->route('barang.index')->with('success', ' Data Mahasiswa Berhasil Di 
        Update');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\barang  $barang
     * @return \Illuminate\Http\Response
     */
    public function destroy(barang $barang)
    {
        $barang->delete();

        return redirect()->route('barang.index')->with('success', 'Data Mahasiswa Berhasil Di
        Hapus');
    }
}
