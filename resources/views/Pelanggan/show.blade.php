@extends('template')

@section('content')
    <div class="row mt-5 mb-5">
        <div class="col-lg-12 margin-tb">
            <div class="float-left">
                <h2> DATA PELANGGAN</h2>
            </div>
            <div class="float-right">
                <a class="btn btn-secondary" href="{{ route('pelanggan.index') }}"> Back</a>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>KODE PELANGGAN:</strong>
                {{ $pelanggan->kode_pelanggan }}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>NAMA PELANGGAN :</strong>
                {{ $pelanggan->nama_pelanggan }}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Alamat:</strong>
                {{ $pelanggan->alamat }}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>NAMA KOTA:</strong>
                {{ $pelanggan->nama_kota }}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>NO TLP:</strong>
                {{ $pelanggan->no_telepon }}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Dibuat pada tanggal:</strong>
                {{ $pelanggan->created_at }}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Di Ubah Pada tanggal:</strong>
                {{ $pelanggan->updated_at }}
            </div>
        </div>
    </div>
@endsection