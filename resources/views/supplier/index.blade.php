@extends('template')

@section('content')
<div class="container mt-5">
        <div class="row">
            <div class="col-md-12">
                <div class="card border-0 shadow rounded">
                <div class="card-body float-left">
                        <h2>Data Supplier Distributor</h2>
                    </div>
                    <div class="card-body">
                        <a href="{{ route('supplier.create') }}" class="btn btn-md btn-success mb-3">INPUT DATA SUPPLIER</a>
                        <table id="example1" class="table table-bordered">
                            <thead>
                              <tr>
                              <th scope="col">no</th>
                                <th scope="col">Kode Supplier</th>
                                <th scope="col">Nama Supplier</th>
                                <th scope="col">Alamat</th>
                                <th scope="col">No Telepone</th>
                                <th scope="col">Action</th>
                              </tr>
                            </thead>
                            <tbody>
                              @forelse ($supplier as $item)
                                <tr>
                                    <td class="text-center">{{ ++$i }}</td>
                                    <td>{{ $item->kode_supplier }}</td>
                                    <td>{{ $item->nama_supplier }}</td>
                                    <td>{{ $item->alamat }}</td>
                                    <td>{{ $item->no_telepon }}</td>
                                    <td class="text-center">
                                        <form onsubmit="return confirm('Apakah Anda Yakin ?');" 
                                        action="{{ route('supplier.destroy', $item->id) }}" method="POST">
                                            
                                            <a href="{{ route('supplier.show', $item->id) }}" 
                                            class="btn btn-info btn-sm"><span class="material-symbols-outlined">info</span></a>
                                            
                                            <a href="{{ route('supplier.edit', $item->id) }}" 
                                            class="btn btn-sm btn-primary"><span class="material-symbols-outlined">edit</span></a>
                                            
                                            @csrf
                                            @method('DELETE')
                                           
                                            <button type="submit" class="btn btn-sm btn-danger"><span 
                                            class="material-symbols-outlined">delete</span></button>
                                        </form>
                                    </td>
                                </tr>
                              @empty
                                  <div class="alert alert-danger">
                                      Data Blog belum Tersedia.
                                  </div>
                              @endforelse
                            </tbody>
                          </table>  
                          {{ $supplier->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
   
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>

    <script>
        //message with toastr
        @if(session()->has('success'))
        
            toastr.success('{{ session('success') }}', 'BERHASIL!'); 

        @elseif(session()->has('error'))

            toastr.error('{{ session('error') }}', 'GAGAL!'); 
            
        @endif
    </script>
   <!--{!! $supplier->links() !!} -->
   <!-- DataTables  & Plugins -->
<script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-buttons/js/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-buttons/js/buttons.bootstrap4.min.js') }}"></script>
<script src="{{ asset('plugins/jszip/jszip.min.js') }}"></script>
<script src="{{ asset('plugins/pdfmake/pdfmake.min.js') }}"></script>
<script src="{{ asset('plugins/pdfmake/vfs_fonts.js') }}"></script>
<script src="{{ asset('plugins/datatables-buttons/js/buttons.html5.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-buttons/js/buttons.print.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-buttons/js/buttons.colVis.min.js')}}"></script>
<!-- Page specific script -->
<script>
  $(function () {
    $("#example1").DataTable({
      "responsive": true, "lengthChange": false, "autoWidth": false,
    }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });
    });
</script>
</html>

@endsection